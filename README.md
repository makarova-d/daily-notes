# "Daily Notes" project overview
It's a "pet" project to get one step closer to the ~~world with rainbows and unicorns~~
scala tech stack.<br />
#### Main features:
- [x] Users sign in/up/out via UI form and oauth2 (gitlab provider)
- [x] Users authentication through http header - bearer token
- [x] CRUD with events. An event is an entity with following fields:
    - text
    - date
    - hours
    - category
    - tags
- [x] Tags creation along with an event via a form on UI
- [x] Events text search with custom limit, offset, order by (request params)
- [x] Events download into a csv file
- [x] Events statistics : event category - total amount of hours
- [x] Swagger ui (yaml)
- [x] Modest UI even with some scala components 


# Running

## Front-end

Install npm dependencies:

```
npm install
```

Build the front-end:

```
sbt frontendJS/fastOptJS
```

Start the webpack dev server:

```
npm start
```

## Back-end

Start sbt:

```
sbt
```

Within sbt start the back-end app:

```
sbt> backend/reStart
```

## Open 

Open http://localhost:30090/ in the browser.

# Developing

To make sbt re-compile the front-end on code changes:

```
sbt ~frontendJS/fastOptJS
```

To re-compile and restart the back-end on code changes:

```
sbt> ~backend/reStart
```