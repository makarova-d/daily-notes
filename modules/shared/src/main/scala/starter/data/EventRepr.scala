package starter.data

import java.time.LocalDate
import io.circe.Decoder.decodeLocalDate
import io.circe.derivation.annotations.JsonCodec

@JsonCodec
final case class EventRepr(
  id: String,
  text: String,
  date: LocalDate,
  hours: Float,
  category: Category,
  tags: List[Tag]
)

@JsonCodec
final case class Category(id: String,
                          name: String,
                          color: String)

@JsonCodec
final case class Tag(id: String,
                     name: String)