package starter.data

import io.circe.derivation.annotations.JsonCodec

@JsonCodec
final case class TokenRepr(access_token: String,
                           token_type: String,
                           expires_in: String)