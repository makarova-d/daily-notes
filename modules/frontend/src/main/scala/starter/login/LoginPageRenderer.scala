package starter.login

import com.raquo.laminar.api.L._
import starter.components.Login

object LoginPageRenderer {

  def render: HtmlElement = Login()

}