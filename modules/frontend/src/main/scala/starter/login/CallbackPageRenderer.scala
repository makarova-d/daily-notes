package starter.login

import com.raquo.laminar.api.L._
import io.circe.parser._
import org.scalajs.dom
import starter.data.{TokenRepr}
import starter.http.{HttpError, Requests}
import starter.pages.CallbackPage

import scala.concurrent.ExecutionContext.Implicits.global
import scala.scalajs.js.Date

object CallbackPageRenderer {

  def render($code: Signal[CallbackPage]): HtmlElement = {
    val content =
      $code.map { page =>
        EventStream.fromFuture(
          Requests.get(url = s"http://localhost:8081/oauth/gitlab/redirect?code=${page.code}",
            headers = Map("Access-Control-Allow-Origin" -> "*")
          ).map { xhr =>
            xhr
          }
        ).map(xhr =>
          if (xhr.status != 201) {
            Left(HttpError(s"${xhr.status} ${xhr.statusText} ${xhr.response}"))
          } else {
            decode[TokenRepr](xhr.responseText)
          }
        ).map {
          case Right(token) => {
            //todo use Cookies.write
            val expires = new Date(token.expires_in).toUTCString
            dom.document.cookie = s"accessToken=${token.access_token}; path=/; expires=$expires"
            dom.window.location.href = "http://localhost:30090/events"
            div()
          }
          case Left(error) => {
            div(
              cls := "bg-red-100 text-red-700",
              span(error.getMessage())
            )
          }
        }
      }
    div(child <-- content.map { c => div(child.maybe <-- c.toWeakSignal) })
  }
}