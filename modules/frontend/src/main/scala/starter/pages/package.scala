package starter

import io.circe.generic.semiauto._

package object pages {

  sealed trait Page {
    def path: String
  }
  case object LoginPage extends Page {
    def path: String = "/signIn"
  }
  case class CallbackPage(code:String) extends Page {
    def path: String = s"/oauth/gitlab/redirect?code=$code"
  }
  case object SettingsPage extends Page {
    def path: String = "/settings"
  }
  case object EventsPage extends Page {
    def path: String = "/events"
  }
  case class ErrorPage(error: String) extends Page {
    def path: String = "/"
  }
  case object NotFoundPage extends Page {
    def path: String = "/"
  }

  object Page {

    implicit val codePage = deriveCodec[Page]

  }
}