package starter.components

import com.raquo.laminar.api.L._
import starter.pages._

object BasePage {

  def apply(content: HtmlElement): HtmlElement =
  //<!-- Page Wrapper -->
    div(idAttr := "wrapper",
      //<!-- Sidebar -->
      ul(cls := "navbar-nav bg-gradient-primary sidebar sidebar-dark accordion",
        idAttr := "accordionSidebar",
        //<!-- Sidebar - Brand -->
        a(cls := "sidebar-brand d-flex align-items-center justify-content-center",
          href := "events.html",
          div(cls := "sidebar-brand-icon rotate-n-15",
            i(cls := "fas fa-book")
          ),
          div(cls := "sidebar-brand-text mx-3", "Daily Notes")
        ),
        //<!-- Divider -->
        hr(cls := "sidebar-divider my-0"),
        //<!-- Nav Item - Dashboard -->
        li(cls := "nav-item",
          a(cls := "nav-link", href := "events",
            i(cls := "fas fa-fw fa-calendar"),
            span(cls := "mx-1 sidebar-text", "Events")
          )
        ),
        li(cls := "nav-item",
          a(cls := "nav-link", href := "stats",
            i(cls := "fas fa-fw fa-align-left"),
            span(cls := "mx-1 sidebar-text", "Statistics")
          )
        ),
        li(cls := "nav-item",
          a(cls := "nav-link", href := "settings",
            i(cls := "fas fa-fw fa-cogs"),
            span(cls := "mx-1 sidebar-text", "Settings")
          )
        )
      ),
      //<!-- End of Sidebar -->

      //<!-- Content Wrapper -->
      div(idAttr := "content-wrapper", cls := "d-flex flex-column",
        // <!-- Main Content -->
        div(idAttr := "content",
          //<!-- Topbar -->
          nav(cls := "navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow",
            // <!-- Topbar Search -->
            form(cls := "d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search",
              div(cls := "input-group",
                input(`type` := "text", cls := "form-control bg-light border-0 small", placeholder := "Search for...",
                  aria.label := "Search", aria.describedBy := "basic-addon2"),
                div(cls := "input-group-append",
                  button(cls := "btn btn-primary", `type` := "button",
                    i(cls := "fas fa-search fa-sm")
                  )
                )
              ),
            ),
            //<!-- Topbar Navbar -->
            ul(cls := "navbar-nav ml-auto",
              UserInfo("John Doe")
            )
        ),
        //<!-- End of Topbar -->

        // <!-- Begin Page Content -->
        div(cls := "container-fluid",
          //<!-- Page Heading -->
          h1(cls := "h3 mb-4 text-gray-800", "Events"),
          content
        ),
      ),
      //<!-- End of Main Content -->

      //<!-- Footer -->
      footer(cls := "sticky-footer bg-white",
        div(cls := "container my-auto",
          div(cls := "copyright text-center my-auto",
            span("Copyright © Daily Notes 2020")
          )
        )
      )
      //<!-- End of Footer -->
    )
  //<!-- End of Content Wrapper -->
  )
}