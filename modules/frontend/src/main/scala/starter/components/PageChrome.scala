package starter.components

import com.raquo.laminar.api.L._

object PageChrome {

  def apply($child: Signal[HtmlElement]): HtmlElement =
    div(child <-- $child)

}