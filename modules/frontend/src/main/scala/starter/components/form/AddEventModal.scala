package starter.components.form

import com.raquo.laminar.api.L._

object AddEventModal {

  def apply(categoriesNames: HtmlElement, tagNames: HtmlElement): HtmlElement =
    div(cls := "modal fade", idAttr("modalAddEventForm"), tabIndex := -1, role := "dialog", aria.labelledBy := "myModalLabel",
      aria.hidden := true,
      div(cls := "modal-dialog", role := "document",
        div(cls := "modal-content",
          div(cls := "modal-header text-center",
            h4(cls := "modal-title w-100 font-weight-bold", "Add an event"),
            button(`type` := "button", cls := "close", dataAttr("dismiss")("modal"), aria.label := "Close",
              span(aria.hidden := true, "×")
            )
          ),
          div(cls := "modal-body mx-3",
            div(cls := "md-form mb-2",
              i(cls := "fas fa-pencil prefix grey-text"),
              input(`type` := "date", idAttr("event-date"), cls := "form-control validate"),
              label(dataAttr("error")("wrong"), dataAttr("success")("right"), forId := "event-date")
            ),
            div(cls := "md-form mb-2",
              i(cls := "fas fa-pencil prefix grey-text"),
              label(forId := "event-hours", "Hours"),
              input(`type` := "number", stepAttr := "0.1", idAttr("event-hours"), cls := "form-control validate")
            ),
            categoriesNames,
            div(cls := "mr-2"),
            div(cls := "md-form",
              i(cls := "fas fa-pencil prefix grey-text"),
              label(forId := "event-text", "Describe your event"),
              textArea(`type` := "text", idAttr("event-text"), cls := "md-textarea form-control", rows := 4),
              label(dataAttr("error")("wrong"), dataAttr("success")("right"), forId := "event-text")
            ),
            tagNames
          ),
          div(cls := "modal-footer d-flex justify-content-center",
            button(cls := "btn btn-success btn-add-event-form", idAttr("add-event-form"),
              span(cls := "text text-white", "Add")
            )
          )
        )
      ),
      script(src := "daily.notes.form.min.js")
    )
}