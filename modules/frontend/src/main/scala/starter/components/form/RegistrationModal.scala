package starter.components.form

import com.raquo.laminar.api.L._

object RegistrationModal {

  def apply(): HtmlElement =
    div(cls := "modal fade", idAttr("modalRegistrationForm"), tabIndex := -1, role := "dialog",
      aria.hidden := true,
      div(cls := "modal-dialog", role := "document",
        div(cls := "modal-content",
          div(cls := "modal-header text-center",
            h4(cls := "modal-title w-100 font-weight-bold", "Create an Account!"),
            button(`type` := "button", cls := "close", dataAttr("dismiss")("modal"), aria.label := "Close",
              span(aria.hidden := true, "×")
            )
          ),
          div(cls := "form-group row",
            div(cls := "col-sm-6 mb-3 mt-3 ml-10",
              input(`type` := "text", idAttr("user-name"), cls := "form-control form-control-user",
                placeholder := "Your Name")
            )
          ),
          div(cls := "form-group row",
            div(cls := "col-sm-6 mb-3 ml-10",
              input(`type` := "email", idAttr("user-email"), cls := "form-control form-control-user",
                placeholder := "Email")
            )
          ),
          div(cls := "form-group row",
            div(cls := "col-sm-6 mb-3 ml-10",
              input(`type` := "password", idAttr("user-password"), cls := "form-control form-control-user",
                placeholder := "Password")
            )
          ),
          div(cls := "container-login100-form-btn mb-3 col-lg-10 ml-5", styleAttr := "",
            button(cls := "login100-form-btn", href := "#", idAttr := "register-user-form", "Register Account")
          )
        )
      ),
      script(src := "registration.form.min.js")
    )
}