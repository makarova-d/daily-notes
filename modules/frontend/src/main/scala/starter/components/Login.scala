package starter.components

import com.raquo.laminar.api.L._
import starter.components.form.RegistrationModal
import starter.util.Icons

object Login {
  def apply(): HtmlElement = {
    div(
      script(src := "login.min.js"),
      div(cls := "limiter",
        div(cls := "container-login100",
          div(cls := "wrap-login100",
            form(cls := "login100-form validate-form",
              span(cls := "login100-form-title p-b-43", "Login to continue"),
              div(cls := "wrap-input100 validate-input", dataAttr("validate").apply("Valid email is required: ex@abc.xyz"),
                input(cls := "input100", `type` := "text", name := "email"),
                span(cls := "focus-input100"),
                span(cls := "label-input100", "Email")
              ),
              div(cls := "wrap-input100 validate-input", dataAttr("validate").apply("Password is required"),
                input(cls := "input100", `type` := "password", name := "pass"),
                span(cls := "focus-input100"),
                span(cls := "label-input100", "Password")
              ),
              div(cls := "container-login100-form-btn",
                button(cls := "login100-form-btn", "Login")
              ),
              div(cls := "text-center p-t-46 p-b-20",
                span(cls := "txt2", "or sign up using")
              ),
              div(cls := "login100-form-social flex-c-m",
                a(cls := "login100-form-social-item flex-c-m bg1 m-r-5", href := "#",
                  Icons.gitlabSquare
                )
              ),
              div(cls := "container-login100-form-btn mt-5",
                button(cls := "login100-form-btn", "Create an Account",
                  dataAttr("toggle")("modal"), dataAttr("target")("#modalRegistrationForm")
                )
              )
            )
          )
        )
      ),
      RegistrationModal()
    )
  }
}