package starter.components

import com.raquo.laminar.api.L._
import starter.data.PostRepr
import starter.util._

object UserInfo {

  def apply(userName: String): HtmlElement =
  //<!-- Nav Item - User Information -->
    li(cls := "nav-item dropdown no-arrow",
      a(cls := "nav-link dropdown-toggle", href := "#",
        idAttr := "userDropdown", role := "button", dataAttr("toggle")("dropdown"),
        aria.hasPopup := true, aria.expanded := false,
        span(cls := "mr-2 d-none d-lg-inline text-gray-600", s"$userName"),
        img(cls := "img-profile rounded-circle", src := "images/default-profile.jpg")
      ),
      //<!-- Dropdown - User Information -->
      div(cls := "dropdown-menu dropdown-menu-right shadow animated--grow-in",
        aria.labelledBy := "userDropdown",
        a(cls := "dropdown-item", href := "#",
          i(cls := "fas fa-user fa-sm fa-fw mr-2 text-gray-400"),
          "Profile"),
        a(cls := "dropdown-item", href := "#",
          i(cls := "fas fa-user fa-sm fa-fw mr-2 text-gray-400"),
          "Settings"),
        div(cls := "dropdown-divider"),
        a(idAttr := "logout", cls := "dropdown-item", href := "#",
          i(cls := "fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"),
          "Logout")
      ),
      script(src := "logout.min.js")
    )
}