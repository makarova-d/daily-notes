package starter.components

import com.raquo.laminar.api.L._

object Pagination {

  def apply(): HtmlElement = {
    div(cls := "row col-lg",
      script(src := "daily.notes.pagination.min.js"),
      div(cls := "pag mb-3", styleAttr := "margin:auto;")
    )
  }

}