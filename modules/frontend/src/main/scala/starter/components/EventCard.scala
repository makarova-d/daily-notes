package starter.components

import com.raquo.laminar.api.L._
import starter.data.EventRepr

object EventCard {

  def apply(event: EventRepr): HtmlElement =
    div(cls := "card shadow h-100 py-2", styleAttr := s"border-left:0.25rem solid ${event.category.color} !important;",
      div(cls := "card-body", dataAttr("category")(event.category.name),
        div(cls := "no-gutters align-items-center",
          div(cls := "col mr-2",
            div(cls := "text-md event-card-date",
              event.date.toString
            ),
            div(cls := "col mb-3"),
            div(cls := "text-md event-card-text",
              event.text
            ),
            div(cls := "col mb-2"),
            div(cls := "text-md event-card-hours",
              f"${event.hours}%1.1f" + "h"
            ),
            div(cls := "col mb-2"),
            div(idAttr := "tag-info",
              event.tags.map(tag =>
                span(cls := "badge badge-primary mr-2 event-card-tag", tag.name)
              )
            )
          )
        ),
        div(cls := "btn-group-right",
          button(idAttr := "event-edit", cls := "btn btn-info btn-circle btn-sm mr-2",
            i(cls := "fas fa-pencil-alt")
          ),
          button(idAttr := "event-delete", cls := "btn btn-danger btn-circle btn-sm mr-2",
            dataAttr("event-id")(event.id),
            i(cls := "fas fa-trash")
          )
        )
      )
    )

}