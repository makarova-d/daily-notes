package starter

import com.raquo.laminar.api.L._
import com.karasiq.markedjs.Marked
import org.scalajs.dom

import scala.scalajs.js.Date

package object util {

  val unsafeMarkdown: UnsafeMarkdownReceiver.type = UnsafeMarkdownReceiver

  object UnsafeMarkdownReceiver {

    def :=(markdown: String): Modifier[HtmlElement] = {
      new Modifier[HtmlElement] {
        override def apply(element: HtmlElement): Unit = element.ref.innerHTML = Marked(markdown)
      }
    }

  }

  object Cookies {
    def read: Map[String, String] =
      dom.document.cookie
        .split(";")
        .toList
        .map(_.split("=").toList)
        .flatMap(x =>
          (x.headOption, x.drop(1).headOption) match {
            case (Some(k), Some(v)) => List((k.trim, v))
            case _                  => Nil
          })
        .toMap

    def write(values:Map[String, String]): Unit = {
      val expiry = new Date(2020, 1)
      values.toList.foreach {
        case (k, v) =>
          val expires = expiry.toUTCString
          dom.document.cookie = s"$k=$v;expires=$expires;path=/"
      }
    }
  }
  
}