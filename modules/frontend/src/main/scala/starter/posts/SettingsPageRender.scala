package starter.posts

import com.raquo.laminar.api.L._
import starter.components.BasePage

object SettingsPageRender {

  def render: HtmlElement = {
    val content = div(idAttr := "demo", cls := "", "Hello",
      script(src := "daily.notes.pagination.min.js")
    )
    BasePage(content)
  }


}