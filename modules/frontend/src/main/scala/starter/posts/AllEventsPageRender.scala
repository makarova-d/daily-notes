package starter.posts

import java.util.UUID

import com.raquo.laminar.api.L._
import com.raquo.laminar.nodes.ReactiveHtmlElement
import io.circe.parser._
import org.scalajs.dom
import org.scalajs.dom.html
import starter.components.form.AddEventModal
import starter.components.{BasePage, EventCard, Pagination}
import starter.data.{Category, EventRepr, Tag}
import starter.http.{HttpError, Requests}
import starter.util.Cookies

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Try

object AllEventsPageRender {

  def render: HtmlElement = {
    val token = Cookies.read.get("accessToken").getOrElse("")
    if (tokenValid(token)) {
      getEvents(token)
    }
    else {
      dom.document.location.href = "/signIn"
      div()
    }
  }

  def tokenValid(token: String): Boolean =
    Try(UUID.fromString(token)).map(_ != null).getOrElse(false)

  private def getEvents(token: String) = {
    val content: EventStream[ReactiveHtmlElement[html.Div]] = EventStream.fromFuture(
      Requests.get(url = "/api/v1/event/list",
        withCredentials = true,
        headers = Map("Access-Control-Allow-Origin" -> "*",
          "Authorization" -> s"Bearer $token")
      ).map { xhr =>
        xhr
      }
    ).map(xhr =>
      if (xhr.status != 200) {
        Left(HttpError(s"${xhr.status} ${xhr.statusText} ${xhr.response}"))
      } else {
        decode[List[EventRepr]](xhr.responseText)
      }
    ).map {
      case Right(events) => renderEvents(events, getCategories(token), getTags(token))
      case Left(error) => getLoginRedirect(error.getMessage())
    }

    div(child <-- content.map { c => BasePage(c) })
  }

  private def getLoginRedirect(errMsg: String) = {
    div(
      cls := "bg-red-100 text-red-700",
      span(errMsg)
    )
    dom.document.location.href = "/signIn"
    div()
  }


  private def renderEvents(events: List[EventRepr], categoriesSelector: HtmlElement, tagPicker: HtmlElement) = {
    div(
      div(cls := "row",
        button(cls := "btn btn-secondary btn-circle btn-sm ml-3",
          dataAttr("toggle")("modal"), dataAttr("target")("#modalAddEventForm"),
          i(cls := "fas fa-plus")
        ),
        button(idAttr := "download-csv", cls := "btn btn-warning btn-circle btn-sm ml-3 mb-2",
          i(cls := "fas fa-arrow-down")
        ),
        AddEventModal(categoriesSelector, tagPicker),
        div(cls := "col-md-6 mb-4", idAttr := "events-column",
          events.map { event =>
            div(cls := "max-w-lg m-4",
              EventCard(event)
            )
          }
        ),
        script(src := "csv.min.js")
      )
    )
  }

  private def renderCategoriesForm(categoriesNames: List[Category]): HtmlElement = {
    div(cls := "md-form mb-2",
      label(forId := "categories-list", "Category : "),
      div(cls := "form-control",
        select(idAttr := "categories-list",
          categoriesNames.map { category =>
            (option(dataAttr("id")(category.id), value := s"${category.name}", s"${category.name}"))
          }
        ))
    )
  }

  private def renderTagsForm(tagNames: List[Tag]): HtmlElement = {
    div(cls := "md-form mb-2",
      label(forId := "tags-list", "Tags : "),
      div(cls := "mr-2"),
      input(idAttr := "tags-list", cls := "form-control", autoComplete := "off", `type` := "text", dataAttr("provide")("typeahead"),
        dataAttr("json")(tagNames.map(tag => s"{'id':'${tag.id}','name':'${tag.name}'}").mkString("[", ", ", "]")),
        placeholder := "Start type in a tag"),
      script("$(function () {" +
        "var data = JSON.parse($('#tags-list').data('json').replace(/'/g,'\"'));" +
        """
          |   var states = new Bloodhound({
          |     datumTokenizer: Bloodhound.tokenizers.obj.whitespace('id', 'name'),
          |     queryTokenizer: Bloodhound.tokenizers.whitespace,
          |     local: data
          |   });
          |   states.initialize();
          |
          |   $("#tags-list").tagsinput({
          |     allowDuplicates: false,
          |     typeaheadjs: {
          |       name: "states",
          |       source: states.ttAdapter(),
          |       displayKey: 'name'//,
          |       //valueKey: 'name'
          |
          |    },
          |    freeInput: true
          |   });
          | });""".stripMargin
      )
    )
  }

  private def getCategories(token: String): HtmlElement = {
    val content = EventStream.fromFuture(
      Requests.get(url = "/api/v1/event/categories",
        withCredentials = true,
        headers = Map("Access-Control-Allow-Origin" -> "*",
          "Authorization" -> s"Bearer $token")
      ).map { xhr =>
        xhr
      }
    ).map(xhr =>
      if (xhr.status != 200) {
        Left(HttpError(s"${xhr.status} ${xhr.statusText} ${xhr.response}"))
      } else {
        decode[List[Category]](xhr.responseText)
      }
    ).map {
      case Right(categories) => renderCategoriesForm(categories)
      case Left(error) => getLoginRedirect(error.getMessage())
    }

    div(child <-- content.map { c => c })
  }

  private def getTags(token: String) = {
    val content = EventStream.fromFuture(
      Requests.get(url = "/api/v1/event/tags",
        withCredentials = true,
        headers = Map("Access-Control-Allow-Origin" -> "*",
          "Authorization" -> s"Bearer $token")
      ).map { xhr =>
        xhr
      }
    ).map(xhr =>
      if (xhr.status != 200) {
        Left(HttpError(s"${xhr.status} ${xhr.statusText} ${xhr.response}"))
      } else {
        decode[List[Tag]](xhr.responseText)
      }
    ).map {
      case Right(tags) => renderTagsForm(tags)
      case Left(error) => getLoginRedirect(error.getMessage())
    }

    div(child <-- content.map { c => c })
  }
}