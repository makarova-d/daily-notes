package starter

import com.raquo.laminar.api.L._
import com.raquo.waypoint._
import com.raquo.waypoint.param
import org.scalajs.dom
import io.circe.syntax._
import io.circe.parser._
import starter.components.BasePage
import starter.pages._
import starter.posts.{AllEventsPageRender, SettingsPageRender}
import starter.login.{CallbackPageRenderer, LoginPageRenderer}
import urldsl.language.QueryParameters.simpleParamErrorImpl._

object Routes {

  val signInRoute = Route.static(LoginPage, root / "signIn" / endOfSegments)
  val callbackGitLabRoute: Route[CallbackPage, String] = Route.onlyQuery(
    encode = page => page.code,
    decode = arg => CallbackPage(arg),
    pattern = (root / "oauth" / "gitlab" / "redirect" / endOfSegments) ? param[String]("code"))

  val settingsRoute = Route.static(SettingsPage, root / "settings" / endOfSegments)
  val eventsRoute = Route.static(EventsPage, root / "events" / endOfSegments)
  val notFoundRoute = Route.static(NotFoundPage, root)

  val router = new Router[Page](
    initialUrl = dom.document.location.href, // must be a valid LoginPage or UserPage url
    origin = dom.document.location.origin.get,
    routes = List(signInRoute, callbackGitLabRoute, eventsRoute, settingsRoute, notFoundRoute),
    owner = unsafeWindowOwner, // this router will live as long as the window
    getPageTitle = _.toString, // mock page title (displayed in the browser tab next to favicon)
    serializePage = page => page.asJson.noSpaces, // serialize page data for storage in History API log
    deserializePage = pageStr => decode[Page](pageStr).fold(e => ErrorPage(e.getMessage), identity) // deserialize the above
  )

  val splitter =
    SplitRender[Page, HtmlElement](router.$currentPage)
      .collectSignal[ErrorPage] { $errorPage =>
        div(
          div("An unpredicted error has just happened. We think this is truly unfortunate."),
          div(
            child.text <-- $errorPage.map(_.error)
          )
        )
      }
      .collectStatic(LoginPage) {
        LoginPageRenderer.render
      }
      .collectSignal[CallbackPage] { $callbackPage =>
        CallbackPageRenderer.render($callbackPage)
      }
      .collectStatic(EventsPage) {
        AllEventsPageRender.render
      }
      .collectStatic(SettingsPage) {
        SettingsPageRender.render
      }
      .collectStatic(NotFoundPage) {
        div("Not Found")
      }

  def pushState(page: Page): Unit = {
    router.pushState(page)
  }

  def replaceState(page: Page): Unit = {
    router.replaceState(page)
  }

  val view = splitter.$view

}