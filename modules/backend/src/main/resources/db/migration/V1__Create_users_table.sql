CREATE TYPE oauth_provider as enum('GitLabProvider');

CREATE TABLE users (
  id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  email VARCHAR(32) NOT NULL UNIQUE,
  password VARCHAR(64),
  name VARCHAR(128),
  oauth_provider oauth_provider,
  oauth_uid INT,
  oauth_access_token VARCHAR(255),
  oauth_refresh_token VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP,
  deleted_at TIMESTAMP);

CREATE UNIQUE INDEX users_email
  ON users (email);