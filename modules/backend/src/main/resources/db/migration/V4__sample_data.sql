INSERT INTO users (email, password) VALUES ('t1@t.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08');
INSERT INTO users (email, password, name) VALUES ('t2@t.com',
'9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'John Doe');
INSERT INTO users (email, password) VALUES ('t3@t.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08');

INSERT INTO categories (name, color) VALUES ('work', '#4e73df');
INSERT INTO categories (name, color) VALUES ('learn', '#36b9cc');
INSERT INTO categories (name, color) VALUES ('personal', '#f6c23e');
INSERT INTO categories (name, color) VALUES ('music', '#ea87b5');
INSERT INTO categories (name, color) VALUES ('family', '#95e7ec');
INSERT INTO categories (name, color) VALUES ('sleep', '#0c3b6a');
INSERT INTO categories (name, color) VALUES ('mood', '#c2d6eb');
INSERT INTO categories (name, color) VALUES ('workout', '#99B898');

INSERT INTO tags (name) VALUES ('on cloud nine');
INSERT INTO tags (name) VALUES ('books');
INSERT INTO tags (name) VALUES ('english');
INSERT INTO tags (name) VALUES ('run');