package org.dailynotes.server.route

import cats.effect.{Blocker, ContextShift, IO}
import io.circe.syntax._
import org.dailynotes.server.domain.User
import org.dailynotes.server.dto.ErrorDTO
import org.dailynotes.server.service.impl.{CsvService}
import org.dailynotes.server.util.JsonUtil._
import org.http4s.{AuthedRoutes, Header, StaticFile}

class CsvRouter(csvService: CsvService, blocker: Blocker)(implicit cs: ContextShift[IO]) extends AuthedRouter {

  def routes: AuthedRoutes[User, IO] =
    AuthedRoutes.of[User, IO] {
      case request@GET -> Path("events") ~ "csv" as user =>
        csvService.getCsvWithEvents(user.id) flatMap {
          case Left(error) => NotFound(error.asJson, Header("Access-Control-Allow-Origin", "*"))
          case Right(file) => StaticFile.fromFile(file, blocker, Some(request.req))
            .getOrElseF(NotFound(ErrorDTO("Cannot serve csv file"), Header("Access-Control-Allow-Origin", "*")))
        }
    }
}