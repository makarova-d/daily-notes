package org.dailynotes.server.repository.impl

import java.sql.Timestamp
import java.time.LocalDate
import java.util.UUID

import cats._
import cats.effect.IO
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import doobie.implicits.javatime._
import doobie.implicits.javasql._
import doobie.util.meta.Meta
import doobie.util.transactor.Transactor
import org.dailynotes.server.dto.{CategoryDTO, CategoryWithHoursDTO, EventDTO, EventWithIdDTO, TagDTO}
import org.dailynotes.server.error.EventError
import org.dailynotes.server.repository.Repository
import org.dailynotes.server.route.EventRouter.OrderByType


final class EventRepository(xa: Transactor[IO]) extends Repository[IO] {

  implicit val han = LogHandler.jdkLogHandler

  implicit val eventDtoRead: Read[EventWithIdDTO] =
    Read[(UUID, String, LocalDate, Float, UUID, String, String, Array[Option[String]])].map {
      case (eventId, text, date, hours, catId, catName, catColor, tagsArr) => {
        val tags: List[TagDTO] = tagsArr.toList.flatten.filter(_.nonEmpty).map { tag =>
          //todo exceptions
          val x = tag.substring(1, tag.length - 1).split(",")
          TagDTO(Some(UUID.fromString(x(0))), Some(x(1)))
        }
        EventWithIdDTO(eventId, text, date, hours, Some(CategoryDTO(catId, Some(catName), Some(catColor))), tags)
      }
    }

  def findAllByUserId(userId: UUID, limit: Int, offset: Int, orderBy: OrderByType): IO[List[EventWithIdDTO]] = {
    val sql = sql"SELECT e.id, e.text, e.event_date, e.hours, c.id, c.name, c.color, array_agg('[' || t.id || ',' || t.name ||']' )::text[] as tags from events e LEFT JOIN categories AS c ON c.id = e.category_id AND c.deleted_at IS NULL LEFT JOIN events_tags AS et ON et.event_id = e.id AND et.deleted_at IS NULL LEFT JOIN tags AS t ON t.id = et.tag_id AND t.deleted_at IS NULL WHERE e.user_id = $userId AND e.deleted_at IS NULL GROUP BY e.id, e.text, e.event_date, e.hours, c.id, c.name, c.color ORDER BY " ++ Fragment.const(s"${orderBy._1} ${orderBy._2}") ++ sql"LIMIT $limit OFFSET $offset"

    sql.query[EventWithIdDTO]
      .to[List]
      .transact(xa)
  }

  def getEventCategories(): IO[List[CategoryDTO]] = {
    sql"SELECT c.id, c.name, c.color FROM categories c WHERE c.deleted_at IS NULL"
      .query[CategoryDTO]
      .to[List]
      .transact(xa)
  }

  def getEventTags(): IO[List[TagDTO]] = {
    sql"SELECT t.id, t.name FROM tags t WHERE t.deleted_at IS NULL"
      .query[TagDTO]
      .to[List]
      .transact(xa)
  }

  def updateEvent(eventDTO: EventWithIdDTO, userId: UUID): IO[Either[EventError, UUID]] = {
    (for {
      eventId <- updateEventById(eventDTO, userId)
      createdTagIds <- insertTags(eventDTO.tags.filter(_.id.isEmpty).map(_.name).flatten)
      _ <- insertEventTags((createdTagIds ::: eventDTO.tags.map(_.id).flatten).map(id => (eventId, id, id, eventId, id)))
      _ <- deleteOutdatedEventTags(eventId, (createdTagIds ::: eventDTO.tags.map(_.id).flatten).map(id => id))
    } yield eventId).attemptSomeSqlState {
      case sqlstate.class23.UNIQUE_VIOLATION => EventError("The id is already exist!")
      case sqlstate.class23.NOT_NULL_VIOLATION => EventError("Null violation error!")
    }.transact(xa)
  }

  def addEvent(eventDTO: EventDTO, userId: UUID): IO[Either[EventError, UUID]] = {
    (for {
      eventId <- insertEvent(eventDTO, userId)
      createdTagIds <- insertTags(eventDTO.tags.filter(_.id.isEmpty).map(_.name).flatten)
      _ <- insertEventTags((createdTagIds ::: eventDTO.tags.map(_.id).flatten).map(id => (eventId, id, id, eventId, id)))
    } yield eventId)
      .attemptSomeSqlState {
        case sqlstate.class23.UNIQUE_VIOLATION => EventError("The id is already exist!")
        case sqlstate.class23.NOT_NULL_VIOLATION => EventError("Null violation error")
      }.transact(xa)
  }

  def deleteEvent(eventId: UUID, userId: UUID): IO[Option[EventError]] = {
    (for {
      event <- deleteEventById(eventId, userId)
      _ <- deleteEventTags(eventId)
    } yield eventId)
      .attemptSomeSqlState {
        case sqlstate.class23.UNIQUE_VIOLATION => EventError("The id is already exist!")
        case sqlstate.class23.NOT_NULL_VIOLATION => EventError("Null violation error")
      }.transact(xa).map {
      case Left(value) => Some(value)
      case Right(value) => None
    }
  }

  def search(query: String, limit: Int, offset: Int, orderBy: OrderByType, userId: UUID): IO[List[EventWithIdDTO]] = {
    val sql = sql"SELECT e.id, e.text, e.event_date, e.hours, c.id, c.name, c.color, array_agg('[' || t.id || ',' || t.name ||']' )::text[] as tags from events e LEFT JOIN categories AS c ON c.id = e.category_id AND c.deleted_at IS NULL LEFT JOIN events_tags AS et ON et.event_id = e.id AND et.deleted_at IS NULL LEFT JOIN tags AS t ON t.id = et.tag_id AND t.deleted_at IS NULL WHERE e.user_id = $userId AND e.deleted_at IS NULL and to_tsvector(text) @@ to_tsquery('" ++ Fragment.const0(s"$query") ++ sql":*') GROUP BY e.id, e.text, e.event_date, e.hours, c.id, c.name, c.color ORDER BY " ++ Fragment.const(s"${orderBy._1} ${orderBy._2}") ++ sql"LIMIT $limit OFFSET $offset"

    sql.query[EventWithIdDTO]
      .to[List]
      .transact(xa)
  }

  def getCategoryStats(userId: UUID, from: LocalDate, to: LocalDate): IO[List[CategoryWithHoursDTO]] = {
    val sql = sql"SELECT c.name, c.color, SUM (e.hours) AS total FROM events e INNER JOIN categories AS c ON c.id = e.category_id WHERE e.user_id = $userId and e.event_date BETWEEN $from AND $to GROUP BY c.name, c.color"

    sql.query[CategoryWithHoursDTO]
      .to[List]
      .transact(xa)
  }


  private def insertEvent(eventDTO: EventDTO, userId: UUID): ConnectionIO[UUID] = {
    sql"INSERT INTO EVENTS (TEXT, EVENT_DATE, HOURS, USER_ID, CATEGORY_ID) VALUES (${eventDTO.text}, ${eventDTO.date}, ${eventDTO.hours}, $userId, ${eventDTO.category.get.id}) RETURNING ID".query[UUID].unique
  }

  private def updateEventById(eventDTO: EventWithIdDTO, userId: UUID): ConnectionIO[UUID] = {
    sql"UPDATE events SET event_date = ${eventDTO.date}, text = ${eventDTO.text}, hours = ${eventDTO.hours}, category_id = ${eventDTO.category.get.id}, updated_at = current_timestamp WHERE id = ${eventDTO.id} AND user_id = $userId AND deleted_at IS NULL RETURNING id".query[UUID].unique
  }

  private def insertEventTags(tags: List[EventTags]) = {
    val sql = "INSERT INTO EVENTS_TAGS (EVENT_ID, TAG_ID) SELECT ?, ? WHERE EXISTS (SELECT tn.deleted_at FROM tags tn WHERE tn.id = ?) AND NOT EXISTS (SELECT et.id FROM EVENTS_TAGS et WHERE et.event_id = ? and et.tag_id = ? and et.deleted_at is null)"
    Update[EventTags](sql).updateMany(tags)
  }

  private def deleteOutdatedEventTags(eventId: UUID, tags: List[UUID]) = {
    val sql = sql"UPDATE events_tags SET deleted_at = current_timestamp where event_id = $eventId and tag_id NOT IN" ++ Fragment.const(s"${tags.mkString("('", "','", "')")}") ++ sql" and deleted_at IS NULL"
    sql.update.run
  }

  private def deleteEventTags(eventId: UUID) = {
    val sql = sql"UPDATE events_tags SET deleted_at = current_timestamp where event_id = $eventId and deleted_at IS NULL"
    sql.update.run
  }

  private def deleteEventById(eventId: UUID, userId: UUID) = {
    val sql = sql"UPDATE events SET deleted_at = current_timestamp where id = $eventId and user_id = $userId and deleted_at IS NULL"
    sql.update.run
  }

  private def insertTags(tags: List[String]): ConnectionIO[List[UUID]] = {
    if (tags.isEmpty) {
      List.empty.pure[ConnectionIO]
    } else {
      val sql = fr"INSERT INTO TAGS (NAME) VALUES" ++ Fragment.const(s"${tags.mkString("('", "'),('", "')")} RETURNING ID")
      sql.query[UUID].to[List]
    }
  }

  type EventTags = (UUID, UUID, UUID, UUID, UUID)
  type EventTag = (UUID, UUID)
}
