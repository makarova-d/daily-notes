package org.dailynotes.server.repository

import java.time.LocalDate

import cats.implicits._
import cats.data.NonEmptyList
import org.dailynotes.server.repository.FieldError._
import org.dailynotes.server.repository.Validator._

trait Validator[T] {
  def validate(target: T): ValidationResult
}

trait FieldValidator[T] {
  def validate(field: T, fieldName: FieldName): ValidationResult
}

object Validator {
  type ValidationResult = Option[NonEmptyList[FieldError]]
}

final case class FieldError(fieldName: FieldName, message: Message)

object FieldError {
  type FieldName = String
  type Message = String
}

case object NotEmpty extends FieldValidator[String] {
  def validate(target: String, fieldName: FieldName) =
    if (target.isEmpty) NonEmptyList.of(FieldError(fieldName, "Must not be empty")).some else None
}

case class WithLength(min: Int, max: Int) extends FieldValidator[String] {
  def validate(target: String, fieldName: FieldName) =
    if (target.length < min || target.length > max)
      NonEmptyList.of(FieldError(fieldName, s"Length must be between $min and $max")).some else None
}

case object PastOrCurrentDate extends FieldValidator[LocalDate] {
  def validate(target: LocalDate, fieldName: FieldName) = {
    val currentDate = LocalDate.now()
    if (target.isAfter(currentDate)) NonEmptyList.of(FieldError(fieldName, "A date must be current or past!")).some else None
  }
}

case object NonNegativeHours extends FieldValidator[Float] {
  def validate(target: Float, fieldName: FieldName) =
    if (target < 0) NonEmptyList.of(FieldError(fieldName, "An hours amount must be greater than 0!")).some else None
}

case object LessTwentyFourHours extends FieldValidator[Float] {
  def validate(target: Float, fieldName: FieldName) =
    if (target.compare(24) > 0) NonEmptyList.of(FieldError(fieldName, "An hours amount must be less than 24!")).some else None
}

case object ValidEmail extends FieldValidator[String] {
  private val emailRegexp = """^[a-zA-Z0-9\.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$""".r

  def validate(target: String, fieldName: FieldName) =
    if (emailRegexp.findFirstMatchIn(target).isDefined) None else
      NonEmptyList.of(FieldError(fieldName, "A email is invalid!")).some
}
