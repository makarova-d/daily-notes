package org.dailynotes.server.service.impl

import java.util.UUID

import cats.effect.IO
import org.dailynotes.server.dto.EventWithIdDTO
import org.dailynotes.server.repository.impl.EventRepository
import org.dailynotes.server.route.ListSearchParams
import org.dailynotes.server.service.RouteService

final class SearchService(repo: EventRepository) extends RouteService[IO] {

  def search(params: ListSearchParams, userId: UUID): IO[List[EventWithIdDTO]] =
    repo.search(params.query, params.limit, params.offset, params.orderBy, userId)
}