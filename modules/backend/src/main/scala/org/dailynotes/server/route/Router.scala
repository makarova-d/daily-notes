package org.dailynotes.server.route

import cats.effect.IO
import com.typesafe.scalalogging.StrictLogging
import org.dailynotes.server.domain.User
import org.http4s.{AuthedRoutes, HttpRoutes}
import org.http4s.dsl.Http4sDsl

trait Router[F[_]]

trait HttpRouter extends Router[IO] with Http4sDsl[IO] with StrictLogging {
  def routes: HttpRoutes[IO]
}

trait AuthedRouter extends Router[IO] with Http4sDsl[IO] with StrictLogging {
  def routes: AuthedRoutes[User, IO]
}