package org.dailynotes.server.domain

case class AccessToken(access_token: String,
                       token_type: String,
                       expires_in: String)

case class OAuth2Token(access_token: String,
                       token_type: String,
                       refresh_token: String,
                       scope: String,
                       created_at: Int)
