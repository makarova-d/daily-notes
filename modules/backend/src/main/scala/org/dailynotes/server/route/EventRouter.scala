package org.dailynotes.server.route

import cats.Applicative
import cats.data.ValidatedNel
import cats.effect.IO
import cats.implicits._
import io.circe.Json
import io.circe.generic.auto._
import io.circe.syntax._
import org.dailynotes.server.domain.User
import org.dailynotes.server.dto.{BaseEventDTO, ErrorDTO, EventDTO, EventWithIdDTO}
import org.dailynotes.server.dto.ErrorDTO.errorDTOJsonEncoder
import org.dailynotes.server.service.impl.EventService
import org.http4s.{AuthedRoutes, Header, ParseFailure, QueryParamDecoder, QueryParameterValue}
import org.http4s.circe._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.dailynotes.server.route.EventRouter._
import org.dailynotes.server.util.JsonUtil.circeIOJsonDecoder

class EventRouter(eventService: EventService) extends AuthedRouter {

  override def routes = AuthedRoutes.of[User, IO] {
    case GET -> Path("event") / "list" :? OptionalValidatingLimitQueryParamMatcher(limit)
      +& OptionalValidatingOffsetQueryParamMatcher(offset)
      +& OptionalValidatingOrderByQueryParamMatcher(orderBy) as user =>

      getParamsOrError(limit, offset, orderBy).map { params =>
        eventService.findAllByUser(user, params.limit, params.offset, params.orderBy) flatMap { events =>
          Ok(events.asJson, Header("Access-Control-Allow-Origin", "*"))
        }
      }.valueOr(BadRequest(_))

    case GET -> Path("event") / "categories" as user =>
      eventService.getEventCategories() flatMap { categories =>
        Ok(categories.asJson, Header("Access-Control-Allow-Origin", "*"))
      }

    case GET -> Path("event") / "tags" as user =>
      eventService.getEventTags() flatMap { tags =>
        Ok(tags.asJson, Header("Access-Control-Allow-Origin", "*"))
      }

    case authedReq@POST -> Path("event") / "add" as user =>
      authedReq.req.decode[EventDTO] { eventDTO =>
        BaseEventDTO.validator.validate(eventDTO) match {
          case Some(errors) => BadRequest(ErrorDTO(errors.map(_.message).mkString_(", ")), Header("Access-Control-Allow-Origin", "*"))
          case None => eventService.addEvent(eventDTO, user.id) flatMap {
            case Left(value) => BadRequest(value.asJson, Header("Access-Control-Allow-Origin", "*"))
            case Right(eventId) => Created(Json.obj("id" -> eventId.asJson), Header("Access-Control-Allow-Origin", "*"))
          }
        }
      }

    case authedReq@PUT -> Path("event") / "update" as user =>
      authedReq.req.decode[EventWithIdDTO] { eventDTO =>
        BaseEventDTO.validator.validate(eventDTO) match {
          case Some(errors) => BadRequest(ErrorDTO(errors.map(_.message).mkString_(", ")), Header("Access-Control-Allow-Origin", "*"))
          case None => eventService.updateEvent(eventDTO, user.id) flatMap {
            case Left(value) => BadRequest(value.asJson, Header("Access-Control-Allow-Origin", "*"))
            case Right(eventId) => Ok(Json.obj("id" -> eventId.asJson), Header("Access-Control-Allow-Origin", "*"))
          }
        }
      }

    case DELETE -> Path("event") / "delete" / UUIDVar(eventId) as user =>
      eventService.deleteEvent(eventId, user.id) flatMap {
        case Some(value) => BadRequest(value.asJson, Header("Access-Control-Allow-Origin", "*"))
        case None => NoContent(Header("Access-Control-Allow-Origin", "*"))
      }
  }
}

object EventRouter {

  case class ListParams(limit: Int, offset: Int, orderBy: (String, String))

  type ErrorOr[ListParams] = ValidatedNel[ParseFailure, ListParams]
  type OrderByType = Tuple2[String, String]

  lazy val DefaultLimit = 10
  lazy val DefaultOffset = 0

  lazy val DefaultOrderBy = ("event_date", "desc")

  private lazy val ValidSortNames = List("asc", "desc")
  private lazy val ValidColumnNames = List("event_date", "hours")

  object OptionalValidatingLimitQueryParamMatcher extends OptionalValidatingQueryParamDecoderMatcher[Int]("limit")

  object OptionalValidatingOffsetQueryParamMatcher extends OptionalValidatingQueryParamDecoderMatcher[Int]("offset")

  object OptionalValidatingOrderByQueryParamMatcher extends OptionalValidatingQueryParamDecoderMatcher[OrderByType]("orderBy")

  implicit lazy val orderByQueryParamDecoder: QueryParamDecoder[OrderByType] = (value: QueryParameterValue) => {
    value.value.split(",") match {
      case Array(col, sort) =>
        if (ValidSortNames.contains(sort.toLowerCase) && ValidColumnNames.contains(col.toLowerCase)) {
          (col, sort).validNel
        } else ParseFailure("Invalid 'orderBy' parameter values!", "Provide another or none").invalidNel
      case _ => ParseFailure("Invalid 'orderBy' parameter!", "Provide another or none").invalidNel
    }
  }

  def getParamsOrError(limit: Option[ValidatedNel[ParseFailure, Int]],
                       offset: Option[ValidatedNel[ParseFailure, Int]],
                       orderBy: Option[ValidatedNel[ParseFailure, (String, String)]]): Either[ErrorDTO, ListParams] = {
    val validatedLimit = limit.getOrElse(DefaultLimit.validNel)
    val validatedOffset = offset.getOrElse(DefaultOffset.validNel)
    val validatedOrderBy = orderBy.getOrElse(DefaultOrderBy.validNel)

    Applicative[ErrorOr].map3(validatedLimit, validatedOffset, validatedOrderBy)(ListParams(_, _, _))
      .valueOr { errs => errs.toList.map(_.sanitized).mkString("\n") } match {
      case errorMsg: String => ErrorDTO(errorMsg).asLeft
      case params: ListParams => params.asRight
    }
  }
}