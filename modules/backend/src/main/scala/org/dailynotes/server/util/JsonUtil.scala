package org.dailynotes.server.util

import cats.effect.IO
import io.circe._
import io.circe.generic.decoding.DerivedDecoder
import io.circe.generic.semiauto._
import io.circe.generic.encoding.DerivedAsObjectEncoder
import org.http4s.circe._
import shapeless.Lazy

object JsonUtil {
  implicit def circeIOJsonDecoder[A](implicit decoder: Decoder[A]) = jsonOf[IO, A]

  implicit def circeIOJsonEncoder[A](implicit encoder: Encoder[A]) = jsonEncoderOf[IO, A]

  implicit def circeJsonEncoder[A](implicit encode: Lazy[DerivedAsObjectEncoder[A]]) = deriveEncoder[A]

  implicit def circeJsonDecoder[A](implicit decode: Lazy[DerivedDecoder[A]]) = deriveDecoder[A]
}