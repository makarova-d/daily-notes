package org.dailynotes.server.service.impl

import java.security.MessageDigest
import java.util.UUID

import cats.effect.IO
import org.dailynotes.server.domain.{AccessToken, User}
import org.dailynotes.server.repository.impl.TokenRepository
import org.dailynotes.server.service.RouteService
import org.dailynotes.server.dto.{UserDirectRegistrationDTO, UserOAuth2RegistrationDTO}
import org.dailynotes.server.error.{TokenError, UserError}

final class AuthService(repo: TokenRepository) extends RouteService[IO] {

  def signUpWithOAuth2(userData: UserOAuth2RegistrationDTO): IO[Either[TokenError, AccessToken]] = {
    repo.createTokenWithOauth(userData)
  }

  def signOut(user: User): IO[Option[TokenError]] = repo.invalidateTokenFor(user.id)

  def findUserByToken(token: UUID): IO[Option[User]] = repo.findUserByToken(token)

  def signUp(userDto: UserDirectRegistrationDTO): IO[Either[UserError, AccessToken]] =
    repo
      .insertUser(repo.createUser(userDto, getSha256Hash(userDto.password)))

  //todo job to delete expires tokens
  def signIn(email: String, password: String): IO[Either[TokenError, AccessToken]] =
    repo.createToken(email, getSha256Hash(password))

  private def getSha256Hash(password: String) = {
    MessageDigest.getInstance("SHA-256")
      .digest(password.getBytes("UTF-8"))
      .map("%02x".format(_)).mkString
  }
}