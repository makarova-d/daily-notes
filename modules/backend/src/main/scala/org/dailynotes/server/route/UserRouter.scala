package org.dailynotes.server.route

import cats.effect.IO
import io.circe.syntax._
import org.dailynotes.server.domain.User
import org.dailynotes.server.route.AuthedRouter
import org.dailynotes.server.service.impl.AuthService
import org.dailynotes.server.util.JsonUtil._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.{AuthedRoutes, Header}

class UserRouter(authService: AuthService) extends AuthedRouter {

  def routes: AuthedRoutes[User, IO] =
    AuthedRoutes.of[User, IO] {
      case GET -> Path("signOut") as user =>
        authService.signOut(user) flatMap {
          case Some(value) => NotFound(value.asJson, Header("Access-Control-Allow-Origin", "*"))
          case None => NoContent(Header("Access-Control-Allow-Origin", "*"))
        }
    }
}