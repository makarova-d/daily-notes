package org.dailynotes.server.route

import cats.Applicative
import cats.data.ValidatedNel
import cats.effect.IO
import cats.implicits._
import io.circe.generic.auto._
import io.circe.syntax._
import org.dailynotes.server.domain.User
import org.dailynotes.server.dto.ErrorDTO.errorDTOJsonEncoder
import org.dailynotes.server.dto.ErrorDTO
import org.dailynotes.server.route.EventRouter.{OptionalValidatingLimitQueryParamMatcher, OptionalValidatingOffsetQueryParamMatcher, OptionalValidatingOrderByQueryParamMatcher, getParamsOrError}
import org.dailynotes.server.service.impl.SearchService
import org.dailynotes.server.util.JsonUtil.circeIOJsonDecoder
import org.http4s.circe._
import org.http4s.dsl.impl.OptionalQueryParamDecoderMatcher
import org.http4s.{AuthedRoutes, Header, ParseFailure}

class SearchRouter(searchService: SearchService) extends AuthedRouter {

  override def routes = AuthedRoutes.of[User, IO] {
    case GET -> Path("search") :? OptionalSearchQueryParamMatcher(query)
      +& OptionalValidatingLimitQueryParamMatcher(limit)
      +& OptionalValidatingOffsetQueryParamMatcher(offset)
      +& OptionalValidatingOrderByQueryParamMatcher(orderBy) as user =>

      getParamsOrError(limit, offset, orderBy, query).map { params =>
        searchService.search(params, user.id) flatMap { events =>
          Ok(events.asJson, Header("Access-Control-Allow-Origin", "*"))
        }
      }.valueOr(BadRequest(_))
  }

  private def getParamsOrError(limit: Option[ValidatedNel[ParseFailure, Int]],
                       offset: Option[ValidatedNel[ParseFailure, Int]],
                       orderBy: Option[ValidatedNel[ParseFailure, (String, String)]],
                       query: Option[String]): Either[ErrorDTO, ListSearchParams] = {
    for {
      params <- EventRouter.getParamsOrError(limit, offset, orderBy)
      searchParams <- Either.cond(query.isDefined,
        ListSearchParams(params.limit, params.offset, params.orderBy, query.get.replace(" ", "")),
        ErrorDTO("No query to search"))
    } yield searchParams
  }
}

case class ListSearchParams(limit: Int, offset: Int, orderBy: (String, String), query: String)

object OptionalSearchQueryParamMatcher extends OptionalQueryParamDecoderMatcher[String]("q")