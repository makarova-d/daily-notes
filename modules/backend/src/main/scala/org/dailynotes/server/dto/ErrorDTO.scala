package org.dailynotes.server.dto

import cats.effect.IO
import io.circe.Encoder
import org.http4s.circe.jsonEncoderOf

case class ErrorDTO(message: String)

object ErrorDTO {
  implicit def errorDTOJsonEncoder(implicit encoder: Encoder[ErrorDTO]) = jsonEncoderOf[IO, ErrorDTO]
}