package org.dailynotes.server.error

import com.akolov.doorman.core.DoormanError

case class OauthProviderError(msg: String) extends DoormanError
