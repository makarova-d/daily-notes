package org.dailynotes.server.domain

case class EmailPasswordCredentials(email: String,
                                    password: String)