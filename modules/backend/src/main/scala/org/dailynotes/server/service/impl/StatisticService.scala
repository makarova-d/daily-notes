package org.dailynotes.server.service.impl

import java.util.UUID

import cats.effect.IO
import org.dailynotes.server.dto.{CategoryWithHoursDTO}
import org.dailynotes.server.repository.impl.EventRepository
import org.dailynotes.server.route.{DatePeriod}
import org.dailynotes.server.service.RouteService

final class StatisticService(repo: EventRepository) extends RouteService[IO] {

  def getCategoryStats(userId: UUID, datePeriod: DatePeriod): IO[List[CategoryWithHoursDTO]] =
    repo.getCategoryStats(userId, datePeriod.from, datePeriod.to)
}