package org.dailynotes.server.middleware

import java.util.UUID

import cats.data.{Kleisli, OptionT}
import cats.effect.IO
import com.typesafe.scalalogging.StrictLogging
import org.dailynotes.server.domain.User
import org.dailynotes.server.service.impl.AuthService
import org.http4s.Credentials.Token
import org.http4s.{AuthScheme, AuthedRoutes, Request, headers}
import org.http4s.dsl.io._
import org.http4s.headers.Authorization
import org.http4s.implicits._
import org.http4s.server.AuthMiddleware

class AuthenticationMiddleware(authService: AuthService) extends StrictLogging{
  val authUser: Kleisli[IO, Request[IO], Either[String, User]] =
    Kleisli({ request =>
      request.headers
        .get(headers.Authorization) match {
        case Some(value) => value match {
          case Authorization(Token(AuthScheme.Bearer, token)) =>
            authService.findUserByToken(UUID.fromString(token))
              .map(_.toRight("A user wasn't found!"))
          case _ => IO.pure(Left("A user wasn't found!"))
        }
        case None => IO.pure(Left("A user wasn't found!"))
      }
    })

  val onFailure: AuthedRoutes[String, IO] = Kleisli(req => OptionT.liftF(Forbidden(req.context)))

  val bearerTokenAuth: AuthMiddleware[IO, User] = AuthMiddleware(authUser, onFailure)
}