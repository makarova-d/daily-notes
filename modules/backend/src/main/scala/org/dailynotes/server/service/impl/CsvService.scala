package org.dailynotes.server.service.impl

import java.io.File
import java.util.UUID

import cats.data.EitherT
import cats.effect.IO
import com.github.tototoshi.csv.CSVWriter
import io.circe.syntax._
import org.dailynotes.server.error.CsvError
import org.dailynotes.server.repository.impl.EventRepository
import org.dailynotes.server.service.RouteService
import org.dailynotes.server.util.JsonUtil._

import scala.util.{Failure, Success, Try}

final class CsvService(repo: EventRepository) extends RouteService[IO] {

  def getCsvWithEvents(userId: UUID): IO[Either[CsvError, File]] = {
    val events = repo.findAllByUserId(userId, 100, 0, ("event_date", "desc"))
    val f = new File("out.csv")
    val writer = CSVWriter.open(f)

    val eventsList = events.unsafeRunSync().map(event => List(event.asJson.noSpaces))
    val writeEvents = Try(writer.writeAll(eventsList))
    writer.close()
    val either: Either[CsvError, File] = writeEvents match {
      case Success(lines) => Right(f)
      case Failure(f) => Left(CsvError("Error while writing a file!"))
    }

    EitherT.fromEither[IO](either).value
  }
}