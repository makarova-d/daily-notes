package org.dailynotes.server.route

import java.time.LocalDate

import cats.Applicative
import cats.data.ValidatedNel
import cats.effect.IO
import cats.implicits._
import io.circe.generic.auto._
import io.circe.syntax._
import org.dailynotes.server.domain.User
import org.dailynotes.server.dto.ErrorDTO.errorDTOJsonEncoder
import org.dailynotes.server.dto.ErrorDTO
import org.dailynotes.server.service.impl.StatisticService
import org.dailynotes.server.route.StatisticRouter._
import org.dailynotes.server.util.JsonUtil.circeIOJsonDecoder
import org.http4s.circe._
import org.http4s.dsl.io.OptionalValidatingQueryParamDecoderMatcher
import org.http4s.{AuthedRoutes, Header, ParseFailure, QueryParamDecoder, QueryParameterValue}

class StatisticRouter(statService: StatisticService) extends AuthedRouter {

  override def routes = AuthedRoutes.of[User, IO] {
    case GET -> Path("stat") / "category" :? OptionalValidatingFromDateQueryParamMatcher(from)
      +& OptionalValidatingToDateQueryParamMatcher(to) as user =>

      getParamsOrError(from, to).map { params =>
        statService.getCategoryStats(user.id, params) flatMap { categories =>
          Ok(categories.asJson, Header("Access-Control-Allow-Origin", "*"))
        }
      }.valueOr(BadRequest(_))

  }
}

object StatisticRouter {

  type ErrorOr[DatePeriod] = ValidatedNel[ParseFailure, DatePeriod]

  implicit lazy val dateQueryParamDecoder: QueryParamDecoder[LocalDate] = (value: QueryParameterValue) => {
    LocalDate.parse(value.value) match {
      case date: LocalDate => date.validNel
      case _ => ParseFailure("Invalid date parameter!", "Provide another or none").invalidNel
    }
  }

  def getParamsOrError(from: Option[ValidatedNel[ParseFailure, LocalDate]],
                       to: Option[ValidatedNel[ParseFailure, LocalDate]]): Either[ErrorDTO, DatePeriod] = {

    val validatedFromPeriod = from.getOrElse(LocalDate.now().minusDays(7).validNel)
    val validatedToPeriod = to.getOrElse(LocalDate.now().validNel)

    Applicative[ErrorOr].map2(validatedFromPeriod, validatedToPeriod)(DatePeriod(_, _))
      .valueOr {
        errs => errs.toList.map(_.sanitized).mkString("\n")
      } match {
      case errorMsg: String => ErrorDTO(errorMsg).asLeft
      case params: DatePeriod => Either.cond(params.to.isAfter(params.from), params, ErrorDTO("From date must be earlier than to date!"))
    }
  }
}

case class DatePeriod(from: LocalDate, to: LocalDate)

object OptionalValidatingFromDateQueryParamMatcher extends OptionalValidatingQueryParamDecoderMatcher[LocalDate]("from")

object OptionalValidatingToDateQueryParamMatcher extends OptionalValidatingQueryParamDecoderMatcher[LocalDate]("to")