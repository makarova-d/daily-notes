package org.dailynotes.server.repository.impl

import java.sql.Timestamp
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.concurrent.TimeUnit
import java.util.UUID

import cats.ApplicativeError
import cats.effect.IO
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.implicits.javasql._
import doobie.postgres.implicits._
import doobie.util.meta.Meta
import doobie.postgres.pgisimplicits._
import doobie.implicits.javasql._
import doobie.postgres._
import doobie.util.transactor.Transactor
import org.dailynotes.server.domain.{AccessToken, OAuthProvider, User}
import org.dailynotes.server.domain.OAuthProvider._
import org.dailynotes.server.repository.Repository
import org.dailynotes.server.dto.{UserDirectRegistrationDTO, UserOAuth2RegistrationDTO}
import org.dailynotes.server.error.{TokenError, UserError, UserNotFoundError}

final class TokenRepository(xa: Transactor[IO]) extends Repository[IO] {

  implicit val logHandler = LogHandler.jdkLogHandler

  implicit val oAuthProviderMeta: Meta[OAuthProvider] =
    pgEnumStringOpt("oauth_provider", OAuthProvider.fromEnum, OAuthProvider.toEnum)

  implicit val oauthProviderPut: Put[OAuthProvider] = Put[String].contramap(toEnum)

  implicit val userRead: Read[User] =
    Read[(UUID, String, Option[String], Option[String], Timestamp)].map {
      case (id, email, pass, name, createdAt) =>
        User(id = id, email = email, password = pass, name = name, created = createdAt)
    }

  def invalidateTokenFor(userId: UUID): IO[Option[TokenError]] = {
    val sql = sql"UPDATE tokens SET deleted_at = current_timestamp where user_id = $userId and deleted_at IS NULL"
    sql.update.run.attemptSqlState.transact(xa).map {
      case Left(sqlState) => Some(TokenError(sqlState.value))
      case Right(value) => None
    }
  }

  def findUserByToken(tokenId: UUID): IO[Option[User]] = {
    sql"SELECT ID, EMAIL, PASSWORD, NAME, CREATED_AT FROM USERS WHERE ID = (SELECT USER_ID FROM TOKENS WHERE ID = $tokenId AND DELETED_AT IS NULL AND EXPIRES_AT > now()) AND DELETED_AT IS NULL"
      .query[User].option.transact(xa)
  }

  def createUser(userDto: UserDirectRegistrationDTO, passwordHash: String): ConnectionIO[Either[String, UUID]] = {
    sql"INSERT INTO USERS (EMAIL, PASSWORD, NAME) VALUES (${userDto.email}, $passwordHash, ${userDto.name}) RETURNING ID"
      .query[UUID]
      .unique
      .attemptSomeSqlState {
        case sqlstate.class23.UNIQUE_VIOLATION => "The id is already exist!"
        case sqlstate.class23.NOT_NULL_VIOLATION => "Null violation"
      }
  }

  private def createUserWithOauth(userData: UserOAuth2RegistrationDTO): ConnectionIO[Either[String, UUID]] = {
    sql"INSERT INTO USERS (EMAIL, NAME, OAUTH_PROVIDER, OAUTH_UID, OAUTH_ACCESS_TOKEN, OAUTH_REFRESH_TOKEN) VALUES (${userData.email}, ${userData.name}, ${userData.oauthProvider}::oauth_provider, ${userData.oauthUid}, ${userData.oAuth2Token.access_token}, ${userData.oAuth2Token.refresh_token}) RETURNING ID"
      .query[UUID]
      .unique
      .attemptSomeSqlState {
        case sqlstate.class23.UNIQUE_VIOLATION => "The id is already exist!"
        case sqlstate.class23.NOT_NULL_VIOLATION => "Null violation"
      }
  }

  def createTokenWithOauth(userData: UserOAuth2RegistrationDTO): IO[Either[TokenError, AccessToken]] = {
    val expiresAt = new Timestamp(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(3600))
    //todo if user already created -> replace accesstoken+refresh token
    (for {
      userId <- userData.oauthUid.map(findUIdByOauthUId(_)).getOrElse(ApplicativeError[ConnectionIO, Throwable].raiseError(UserNotFoundError("A user id from oauth provider is absent!")))
      token <- userId.map { id => safeInsertTokenById(id, expiresAt) }
        .getOrElse(createUserWithToken(createUserWithOauth(userData)))
    } yield token).transact(xa).map {
      case Left(value) => Left(TokenError(value))
      case Right(value) => Right(AccessToken(value.toString, "bearer", expiresAt.toString))
    }
  }

  private def createUserWithToken(createUser: => ConnectionIO[Either[String, UUID]]): ConnectionIO[Either[String, UUID]] = {
    val expiresAt = new Timestamp(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(3600))

    for {
      userId <- createUser
      token <- userId.map { id => safeInsertTokenById(id, expiresAt) }
        .getOrElse(userId.pure[ConnectionIO])
    } yield token
  }

  def findUIdByOauthUId(uid: Int): ConnectionIO[Option[UUID]] = {
    sql"SELECT ID FROM USERS WHERE OAUTH_UID = $uid AND DELETED_AT IS NULL".query[UUID].option
  }

  def insertUser(createUser: => ConnectionIO[Either[String, UUID]]): IO[Either[UserError, AccessToken]] = {
    val expiresAt = new Timestamp(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(3600))

    (for {
      userId <- createUser
      token <- userId.map { id => safeInsertTokenById(id, expiresAt) }
        .getOrElse(userId.pure[ConnectionIO])
    } yield token).transact(xa).map {
      case Left(value) => Left(UserError(value))
      case Right(value) => Right(AccessToken(value.toString, "bearer", expiresAt.toString))
    }
  }

  //todo from config
  lazy val tokenLifeTimeInSeconds = 3600

  private def getUTCDate(datetime: Timestamp): OffsetDateTime = {
    OffsetDateTime.ofInstant(datetime.toInstant, ZoneOffset.UTC)
  }

  def createToken(email: String, password: String): IO[Either[TokenError, AccessToken]] = {
    val expiresAt = new Timestamp(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(3600))

    (for {
      _ <- invalidateActiveUserTokens(email, password)
      token <- safeInsertToken(email, password, expiresAt)
    } yield token).transact(xa).map {
      case Left(value) => Left(TokenError(value))
      case Right(value) => Right(AccessToken(value.toString, "bearer", getUTCDate(expiresAt).toString))
    }
  }

  private def invalidateActiveUserTokens(email: String, password: String): ConnectionIO[Int] = {
    val deletedTimestamp = new Timestamp(System.currentTimeMillis())
    sql"UPDATE TOKENS SET DELETED_AT = $deletedTimestamp WHERE DELETED_AT IS NULL and USER_ID = (SELECT ID FROM USERS WHERE EMAIL = $email and PASSWORD = $password)"
      .update
      .run
  }

  private def insertToken(email: String, password: String, expiresAt: Timestamp): ConnectionIO[UUID] = {
    sql"INSERT INTO TOKENS (EXPIRES_AT, USER_ID) VALUES ($expiresAt, (SELECT ID FROM USERS WHERE EMAIL = $email and PASSWORD = $password and DELETED_AT IS NULL)) RETURNING ID"
      .query[UUID].unique
  }

  private def safeInsertToken(email: String, password: String, expiresAt: Timestamp): ConnectionIO[Either[String, UUID]] =
    insertToken(email, password, expiresAt).attemptSomeSqlState {
      case sqlstate.class23.UNIQUE_VIOLATION => "The id is already exist!"
      case sqlstate.class23.NOT_NULL_VIOLATION => "Null violation"
    }


  private def safeInsertTokenById(userId: UUID, expiresAt: Timestamp): ConnectionIO[Either[String, UUID]] = {
    sql"INSERT INTO TOKENS (EXPIRES_AT, USER_ID) VALUES ($expiresAt, $userId) RETURNING ID"
      .query[UUID]
      .unique
      .attemptSomeSqlState {
        case sqlstate.class23.UNIQUE_VIOLATION => "The id is already exist!"
        case sqlstate.class23.NOT_NULL_VIOLATION => "Null violation"
      }
  }
}