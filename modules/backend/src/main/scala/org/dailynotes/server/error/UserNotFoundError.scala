package org.dailynotes.server.error

import java.lang.Exception

import scala.util.control.NoStackTrace

//useless stacktraces for cats-effect
case class UserNotFoundError(msg: String) extends Exception(msg) with NoStackTrace
