package org.dailynotes.server.route

import cats.effect.IO
import org.dailynotes.server.service.impl.AuthService
import org.dailynotes.server.provider.OAuthProvider
import org.dailynotes.server.util.JsonUtil._
import org.http4s.{Header, HttpRoutes}
import org.http4s.circe.encodeUri
import org.http4s.client.Client
import io.circe.syntax._

class Oauth2Router(authService: AuthService, httpClient: Client[IO]) extends HttpRouter {

  //todo logger error
  override val routes: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root / "oauth" / "gitlab" =>
      OAuthProvider.getAuthUrl() match {
        case Left(error) => NotFound("Cannot construct an url!", Header("Access-Control-Allow-Origin", "*"))
        case Right(uri) => Created(uri.asJson, Header("Access-Control-Allow-Origin", "*"))
      }

    case GET -> Root / "oauth" / "gitlab" / "redirect" :? CodeQueryParamMatcher(code) =>
      OAuthProvider.getOauthToken(code, httpClient) flatMap {
        case Left(value) => NotFound(Header("Access-Control-Allow-Origin", "*"))
        case Right(userData) => authService.signUpWithOAuth2(userData) flatMap {
          case Left(value) => NotFound(value.asJson, Header("Access-Control-Allow-Origin", "*"))
          case Right(token) => Created(token.asJson, Header("Access-Control-Allow-Origin", "*"))
        }
      }
  }

  object CodeQueryParamMatcher extends QueryParamDecoderMatcher[String]("code")

}