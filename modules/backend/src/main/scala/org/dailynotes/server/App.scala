package org.dailynotes.server

import cats.data.{Kleisli, OptionT}
import cats.effect._
import cats.implicits._
import com.typesafe.scalalogging.StrictLogging
import org.dailynotes.server.config.ConfigComponent
import org.dailynotes.server.config.ConfigComponent.DatabaseConfig
import org.dailynotes.server.db.{DatabaseMigrator, FlywayDatabaseMigrator}
import org.dailynotes.server.middleware.AuthenticationMiddleware
import org.dailynotes.server.repository.impl.{EventRepository, TokenRepository}
import org.dailynotes.server.route.{CsvRouter, EventRouter, Oauth2Router, SearchRouter, StatisticRouter, SwaggerRouter, UnauthRouter, UserRouter}
import org.dailynotes.server.service.impl.{AuthService, CsvService, EventService, SearchService, StatisticService}
import org.http4s.{AuthedRoutes, HttpApp, HttpRoutes, Request}
import org.http4s.implicits._
import org.http4s.client.JavaNetClientBuilder
import org.http4s.server.Router
import org.http4s.server.middleware.{CORS, CORSConfig, Logger, Metrics}

object App extends IOApp with StrictLogging {
  //todo to config
  private lazy val apiContextPath = "/api/v1"
  private lazy val corsConfig: CORSConfig = CORS.DefaultCORSConfig

  private lazy val migrator: DatabaseMigrator[IO] = new FlywayDatabaseMigrator

  override def run(args: List[String]): IO[ExitCode] = {

    val resources = for {
      config <- ConfigComponent[IO]()
      blocker <- Blocker[IO]
      transactor <- DatabaseConfig.transactor[IO](config.database, blocker)
      _ <- Resource.liftF(migrator.migrate(transactor))
      //todo async
      httpClient = JavaNetClientBuilder[IO](blocker).create
      tokenRepo = new TokenRepository(transactor)
      eventRepo = new EventRepository(transactor)
      authService = new AuthService(tokenRepo)
      eventService = new EventService(eventRepo)
      searchService = new SearchService(eventRepo)
      statService = new StatisticService(eventRepo)
      csvService = new CsvService(eventRepo)
      authMiddleware = new AuthenticationMiddleware(authService)
      docsRoutes = new SwaggerRouter(config, blocker)
      unAuthRoutes = new UnauthRouter(authService).routes <+>
        new Oauth2Router(authService, httpClient).routes <+> docsRoutes.routes
      apiRoutes = authMiddleware.bearerTokenAuth(
        new UserRouter(authService).routes <+> new EventRouter(eventService).routes
          <+> new SearchRouter(searchService).routes <+> new CsvRouter(csvService, blocker).routes
          <+> new StatisticRouter(statService).routes)

      server <- {
        val app: HttpApp[IO] = Router(
          s"$apiContextPath" -> (CORS(apiRoutes, corsConfig)),
          "/" -> (CORS(unAuthRoutes, corsConfig))
        ).orNotFound
        ServerComponent(config)(app)
      }
    } yield server

    resources.use(_ => IO.never).as(ExitCode.Success)
  }
}
