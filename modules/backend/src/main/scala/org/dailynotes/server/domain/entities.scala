package org.dailynotes.server.domain

import java.sql.Timestamp
import java.time.LocalDate
import java.util.UUID

import enumeratum._

case class User(id: UUID,
                email: String,
                password: Option[String] = None,
                name: Option[String] = None,
                oauthProvider: Option[OAuthProvider] = None,
                oauthUid: Option[Int] = None,
                oauthAccessToken: Option[String] = None,
                oauthRefreshToken: Option[String] = None,
                created: Timestamp,
                updated: Option[Timestamp] = None,
                deleted: Option[Timestamp] = None)

sealed trait OAuthProvider extends EnumEntry

object OAuthProvider extends Enum[OAuthProvider] with DoobieEnum[OAuthProvider] {

  case object GitLabProvider extends OAuthProvider

  val values = findValues

  def toEnum(provider: OAuthProvider): String = {
    provider.entryName
  }

  def fromEnum(provider: String): Option[OAuthProvider] = {
    Option(provider) collect {
      case "GitLabProvider" => GitLabProvider
    }
  }
}