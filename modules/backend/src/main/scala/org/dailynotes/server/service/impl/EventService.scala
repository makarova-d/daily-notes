package org.dailynotes.server.service.impl

import java.util.UUID

import cats.effect.IO
import org.dailynotes.server.domain.User
import org.dailynotes.server.dto.{CategoryDTO, EventDTO, EventWithIdDTO, TagDTO}
import org.dailynotes.server.error.EventError
import org.dailynotes.server.repository.impl.EventRepository
import org.dailynotes.server.route.EventRouter.OrderByType
import org.dailynotes.server.service.RouteService

final class EventService(repo: EventRepository) extends RouteService[IO] {

  def addEvent(eventDTO: EventDTO, userId: UUID): IO[Either[EventError, UUID]] =
    repo.addEvent(eventDTO, userId)

  def updateEvent(eventDTO: EventWithIdDTO, userId: UUID): IO[Either[EventError, UUID]] =
    repo.updateEvent(eventDTO, userId)

  def deleteEvent(eventId: UUID, userId: UUID): IO[Option[EventError]] =
    repo.deleteEvent(eventId, userId)

  def findAllByUser(user: User, limit: Int, offset: Int, orderBy: OrderByType): IO[List[EventWithIdDTO]] = {
    repo.findAllByUserId(user.id, limit, offset, orderBy)
  }

  def getEventCategories(): IO[List[CategoryDTO]] = repo.getEventCategories()

  def getEventTags(): IO[List[TagDTO]] = repo.getEventTags()
}
