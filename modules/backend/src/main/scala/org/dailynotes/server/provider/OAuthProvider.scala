package org.dailynotes.server.provider

import cats.data._
import cats.effect._
import cats.implicits._
import com.akolov.doorman.core.{ConfigurationError, DoormanError, NoAccessTokenInResponse, OAuthEndpoints, OAuthProviderConfig, UserData, UserManager}
import io.circe._
import org.dailynotes.server.error.OauthProviderError
import org.http4s.headers.{Accept, AgentProduct, Authorization, `User-Agent`}
import org.dailynotes.server.dto.{UserOAuth2RegistrationDTO}
import org.dailynotes.server.domain.OAuth2Token
import org.http4s._
import org.http4s.client.Client
import org.http4s.client.dsl.io._
import org.http4s.Method._
import org.dailynotes.server.util.JsonUtil._

class OAuthEndpointsProvider {

  def login(config: OAuthProviderConfig): Either[DoormanError, Uri] =
    OAuthEndpoints[IO]().login(OAuthProvider.oauthConfig)

  def callback(config: OAuthProviderConfig, code: String, client: Client[IO]): IO[Either[DoormanError, UserOAuth2RegistrationDTO]] = {
    (for {
      uri <- getUri(config.accessTokenUri)
      //without user-agent gitlab returns response 403 with error code 1010
      request = POST(tokenUrlForm(config, code), uri, `User-Agent`(AgentProduct("PostmanRuntime", Some("7.24.1"))), Accept(MediaType.application.json))
      resp <- EitherT.liftF[IO, DoormanError, OAuth2Token](client.expect[OAuth2Token](request))
      token <-  EitherT.fromOption[IO](Some(resp), NoAccessTokenInResponse())
      uriUser <- getUri(config.userInfoUri)
      respUser <- EitherT.liftF[IO, DoormanError, JsonObject] {
        client.expect[JsonObject](getUserInfoRequest(uriUser, token.access_token))
      }
      userRegistrationDTO <- EitherT.fromOption[IO](getUserRegistrationDTO(respUser.toMap, token), OauthProviderError("A user oauth registration entity is absent!"))
      userMap <- EitherT.pure[IO, DoormanError](userRegistrationDTO)
    } yield userMap).value
  }

  private def getUri(path: String) = {
    EitherT.fromEither[IO](
      Uri.fromString(path).leftMap(e => ConfigurationError(e.message))
    )
  }

  private def tokenUrlForm(config: OAuthProviderConfig, code: String) = UrlForm(
    ("redirect_uri", config.redirectUrl),
    ("client_id", config.clientId),
    ("client_secret", config.clientSecret),
    ("code", code),
    ("grant_type", "authorization_code")
  )

  private def getUserInfoRequest(uri: Uri, token: String) = Request[IO](
    method = GET,
    uri = uri,
    headers = Headers.of(
      Accept(MediaType.application.json),
      Authorization(Credentials.Token(AuthScheme.Bearer, token))
    )
  )

  private def getUserRegistrationDTO(userData: Map[String, Json], token: OAuth2Token): Option[UserOAuth2RegistrationDTO] = {
    for {
      email <- userData.get("email")
      idJson <- userData.get("id")
      oauthUid <- idJson.asNumber
      name <- userData.get("name")
    } yield UserOAuth2RegistrationDTO(email = email.asString, oauthUid = oauthUid.toInt, oAuth2Token = token, name = name.asString)
  }

}


object OAuthProvider {

  //todo from config
  lazy val oauthConfig = OAuthProviderConfig(
    "https://gitlab.com/oauth/authorize",
    "https://gitlab.com/oauth/token",
    "https://gitlab.com/api/v4/user",
    "3d923ee67a1f325700a9119bc19c655c7cb18fc289f3f4153f4f7a9a48e9a3fb",
    "8ded4aa0ee42bf446972ad139e638d00a672afefa3188700d6f8abee40cac09f",
    List("read_user", "read_api"),
    "http://localhost:30090/oauth/gitlab/redirect"
  )

  def getAuthUrl() = {
    OAuthEndpoints[IO]().login(oauthConfig)
  }

  def getOauthToken(code: String, httpClient: Client[IO]) = {
    new OAuthEndpointsProvider().callback(oauthConfig, code, httpClient)
  }
}