package org.dailynotes.server

import cats.effect.{ConcurrentEffect, Resource, Timer}
import org.dailynotes.server.config.ConfigComponent.ApplicationConfig
import org.http4s.{HttpApp, HttpRoutes}
import org.http4s.implicits._
import org.http4s.server.{Router, Server}
import org.http4s.server.blaze.BlazeServerBuilder

object ServerComponent {
  def apply[F[_]: ConcurrentEffect: Timer](config: ApplicationConfig)(app: HttpApp[F]): Resource[F, Server[F]] = {
    BlazeServerBuilder[F]
      .bindHttp(config.server.port, config.server.host)
      .withHttpApp(app)
      .resource
  }
}
