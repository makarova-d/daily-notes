package org.dailynotes.server.dto

import cats.implicits._

import org.dailynotes.server.domain.OAuthProvider.GitLabProvider
import org.dailynotes.server.domain.{OAuth2Token, OAuthProvider}
import org.dailynotes.server.repository.{ValidEmail, Validator, WithLength}

sealed trait UserRegistrationDTO

case class UserDirectRegistrationDTO(email: String,
                                     password: String,
                                     name: Option[String]) extends UserRegistrationDTO
object UserRegistrationDTO {

  implicit val validator: Validator[UserDirectRegistrationDTO] = (target: UserDirectRegistrationDTO) => {
    WithLength(1, 1000).validate(target.password, "password") |+|
    ValidEmail.validate(target.email, "email")
  }
}

case class UserOAuth2RegistrationDTO(email: Option[String] = None,
                                     oauthProvider: OAuthProvider = GitLabProvider,
                                     oauthUid: Option[Int] = None,
                                     oAuth2Token: OAuth2Token,
                                     name: Option[String] = None) extends UserRegistrationDTO