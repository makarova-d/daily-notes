package org.dailynotes.server.config

import cats.effect.{Async, Blocker, ContextShift, Resource, Sync}
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts
import pureconfig._
import pureconfig.generic.auto._

object ConfigComponent {

  final case class ServerConfig(host: String, port: Int)

  final case class DatabaseConfig(driver: String, url: String, user: String, password: String, threadPoolSize: Int)

  final case class ApplicationConfig(server: ServerConfig, database: DatabaseConfig)

  def apply[F[_] : Sync](): Resource[F, ApplicationConfig] = {
    Resource.liftF[F, ApplicationConfig](Sync[F].delay(ConfigSource.default.loadOrThrow[ApplicationConfig]))
  }

  object DatabaseConfig {
    def transactor[F[_] : Async : ContextShift](config: DatabaseConfig, blocker: Blocker) = {
      ExecutionContexts.fixedThreadPool[F](config.threadPoolSize).flatMap { ce =>
        HikariTransactor.newHikariTransactor[F](
          config.driver,
          config.url,
          config.user,
          config.password,
          ce,
          blocker
        )
      }
    }
  }
}