package org.dailynotes.server.db

import cats.effect.IO
import doobie.hikari.HikariTransactor
import org.flywaydb.core.Flyway

trait DatabaseMigrator[F[_]] {
  def migrate(transactor: HikariTransactor[F]): F[Int]
}

final class FlywayDatabaseMigrator extends DatabaseMigrator[IO] {
  override def migrate(transactor: HikariTransactor[IO]): IO[Int] = {
    transactor.configure { dataSource =>
      IO {
        val flyWay = Flyway.configure().dataSource(dataSource).load()
        flyWay.migrate()
      }
    }
  }
}