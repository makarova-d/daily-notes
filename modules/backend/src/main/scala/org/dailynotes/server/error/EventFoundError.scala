package org.dailynotes.server.error

import scala.util.control.NoStackTrace

//useless stacktraces for cats-effect
case object EventFoundError extends Exception with NoStackTrace
