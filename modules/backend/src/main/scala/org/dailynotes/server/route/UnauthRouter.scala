package org.dailynotes.server.route

import cats.implicits._
import cats.effect.IO
import org.dailynotes.server.service.impl.AuthService
import org.dailynotes.server.dto.{ErrorDTO, UserDirectRegistrationDTO, UserRegistrationDTO}
import org.dailynotes.server.domain.{EmailPasswordCredentials}
import org.http4s.dsl.io.{BadRequest, Created}
import org.http4s.{Header, HttpRoutes}
import io.circe.syntax._
import io.circe.generic.auto._
import org.http4s.circe._
import org.dailynotes.server.util.JsonUtil.circeIOJsonDecoder

class UnauthRouter(authService: AuthService) extends HttpRouter {

  override val routes = HttpRoutes.of[IO] {
    case req@POST -> Root / "signIn" =>
      req.decode[EmailPasswordCredentials] { credentials =>
        logger.debug(s"SignIn for user: ${credentials.email}")
        authService.signIn(credentials.email, credentials.password) flatMap {
          case Left(value) => NotFound(value.asJson, Header("Access-Control-Allow-Origin", "*"))
          case Right(token) => Created(token.asJson, Header("Access-Control-Allow-Origin", "*"))
        }
      }

    case req@POST -> Root / "signUp" =>
      req.decode[UserDirectRegistrationDTO] { userDTO =>
        UserRegistrationDTO.validator.validate(userDTO) match {
          case Some(errors) => BadRequest(ErrorDTO(errors.map(_.message).mkString_(", ")), Header("Access-Control-Allow-Origin", "*"))
          case None => authService.signUp(userDTO) flatMap {
            case Left(value) => BadRequest(value.asJson, Header("Access-Control-Allow-Origin", "*"))
            case Right(token) => Created(token.asJson, Header("Access-Control-Allow-Origin", "*"))
          }
        }
      }
  }
}