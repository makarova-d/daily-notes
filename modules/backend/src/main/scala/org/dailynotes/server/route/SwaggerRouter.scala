package org.dailynotes.server.route

import cats.data.OptionT
import cats.implicits._
import cats.effect.{Blocker, ContextShift, IO}
import io.circe.Json
import io.circe.generic.auto._
import io.circe.syntax._
import org.dailynotes.server.config.ConfigComponent.ApplicationConfig
import org.dailynotes.server.domain.User
import org.http4s.circe._
import org.http4s.dsl.impl.Path
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.io._
import org.http4s.headers.Location
import org.http4s.implicits._
import org.http4s.{HttpRoutes, StaticFile}
import org.webjars.WebJarAssetLocator

class SwaggerRouter(config: ApplicationConfig, blocker: Blocker)(implicit cs: ContextShift[IO]) {

  private val applicationUrl = s"http://${config.server.host}:${config.server.port}"
  private val swaggerUiPath = Path("swagger-ui")

  def routes: HttpRoutes[IO] = HttpRoutes[IO] {
    case req@GET ->  Path("swagger.yaml") =>
      StaticFile.fromResource("/swagger-ui/swagger.yaml", blocker, req = Some(req))

    case GET -> swaggerUiPath / "config.json"=>
      //Specifies Swagger spec URL
      OptionT.liftF(Ok(Json.obj("url" -> Json.fromString(s"$applicationUrl/swagger.yaml"))))
    //Entry point to Swagger UI

    case req@GET -> path if path.startsWith(swaggerUiPath) =>
      //Serves Swagger UI files
      val file = "/" + path.toList.drop(swaggerUiPath.toList.size).mkString("/")
      val resp = (if (file == "/index.html") {
        StaticFile.fromResource("/swagger-ui/index.html", blocker, req = Some(req))
      } else {
        StaticFile.fromResource(swaggerUiResources + file, blocker , req = Some(req))
      }).getOrElseF(NotFound())
      OptionT.liftF(resp)
  }

  private val swaggerUiResources = s"/META-INF/resources/webjars/swagger-ui/$swaggerUiVersion"

  private lazy val swaggerUiVersion: String = {
    Option(new WebJarAssetLocator().getWebJars.get("swagger-ui")).fold {
      throw new RuntimeException(s"Could not detect swagger-ui webjar version")
    } { version =>
      version
    }
  }

}