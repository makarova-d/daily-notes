package org.dailynotes.server.error

import scala.util.control.NoStackTrace

//useless stacktraces for cats-effect
case class UserError(msg: String) extends Exception(msg) with NoStackTrace
