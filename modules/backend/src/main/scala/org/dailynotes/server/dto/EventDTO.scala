package org.dailynotes.server.dto

import java.time.LocalDate
import java.util.UUID

import cats.implicits._
import org.dailynotes.server.repository.{LessTwentyFourHours, NonNegativeHours, NotEmpty, PastOrCurrentDate, Validator, WithLength}

sealed abstract class BaseEventDTO {
  def text: String
  def date: LocalDate
  def hours: Float
  def category: Option[CategoryDTO]
  def tags: List[TagDTO]
}

case class EventDTO(id: Option[UUID] = None,
                    text: String,
                    date: LocalDate,
                    hours: Float,
                    category: Option[CategoryDTO],
                    tags: List[TagDTO]) extends BaseEventDTO

case class EventWithIdDTO(id: UUID,
                          text: String,
                          date: LocalDate,
                          hours: Float,
                          category: Option[CategoryDTO],
                          tags: List[TagDTO]) extends BaseEventDTO

object BaseEventDTO {

  implicit def validator[T <: BaseEventDTO]: Validator[T] = (target: BaseEventDTO) => {
    WithLength(1, 1000).validate(target.text, "text") |+|
      PastOrCurrentDate.validate(target.date, "date") |+|
      NonNegativeHours.validate(target.hours, "hours") |+|
      LessTwentyFourHours.validate(target.hours, "hours")
  }
}

case class CategoryDTO(id: UUID,
                       name: Option[String],
                       color: Option[String])

case class CategoryWithHoursDTO(name: String,
                                color: String,
                                totalHours: Float)

case class TagDTO(id: Option[UUID],
                  name: Option[String])