import org.portablescala.sbtplatformdeps.PlatformDepsPlugin.autoImport._
import sbt._

object Settings {

  object versions {
    val scala = "2.13.1"
    val laminar = "0.9.0"
    val `url-dsl` = "0.2.0"
    val waypoint = "0.1.0"
    val http4sVersion = "0.21.3"
    val doobieVersion = "0.8.8"
    val flywayVersion = "6.3.3"
    val circe = "0.13.0"
    val `circe-derivation` = "0.13.0-M4"
    val `typesafe-config` = "1.4.0"
    val pureconfig = "0.12.3"
    val enumeratum = "1.6.0"
    val uTest = "0.6.6"
    val `dom-test-utils` = "0.12.0"
  }

  val scalacOptions = Seq(
    "-target:jvm-1.8",
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Xlint:nullary-unit,inaccessible,nullary-override,infer-any,missing-interpolator,private-shadow,type-parameter-shadow,poly-implicit-overload,option-implicit,delayedinit-select,stars-align",
    "-Xcheckinit",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-Ywarn-value-discard",
    // for Scala 2.13 only
    "-Ymacro-annotations",
    // ---
    "-encoding",
    "utf8"
  )

  object libs {

    val laminar: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "com.raquo" %%% "laminar" % versions.laminar
      )
    }

    val `url-dsl`: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "be.doeraene" %%% "url-dsl" % versions.`url-dsl`
      )
    }

    val waypoint: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "com.raquo" %%% "waypoint" % versions.waypoint
      )
    }

    val jquery: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "org.webjars" % "jquery" % "2.1.4"
      )
    }

    val `scalajs-dom`: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "org.scala-js" %%% "scalajs-dom" % "1.0.0"

      )
    }

    val uTest: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "com.lihaoyi" %%% "utest" % versions.uTest
      )
    }

    val logging: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "ch.qos.logback" % "logback-classic" % "1.2.3",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
      )
    }

    val http4s: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "org.http4s" %% "http4s-blaze-server" % versions.http4sVersion,
        "org.http4s" %% "http4s-blaze-client" % versions.http4sVersion,
        "org.http4s" %% "http4s-circe" % versions.http4sVersion,
        "org.http4s" %% "http4s-dsl" % versions.http4sVersion
      )
    }

    val oauth2: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "com.akolov" %% "doorman" % "0.4.0"
      )
    }

    val `typesafe-config`: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "com.typesafe" % "config" % versions.`typesafe-config`
      )
    }

    val pureconfig: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "com.github.pureconfig" %%% "pureconfig" % versions.pureconfig,
        "com.github.pureconfig" %% "pureconfig-cats-effect" % versions.pureconfig
      )
    }

    val circe: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "io.circe" %%% "circe-core" % versions.circe,
        "io.circe" %%% "circe-generic" % versions.circe,
        "io.circe" %%% "circe-derivation" % versions.`circe-derivation`,
        "io.circe" %%% "circe-derivation-annotations" % versions.`circe-derivation`,
        "io.circe" %%% "circe-parser" % versions.circe
      )
    }

    val doobie: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "org.tpolecat" %% "doobie-core" % versions.doobieVersion,
        "org.tpolecat" %% "doobie-h2" % versions.doobieVersion,
        "org.tpolecat" %% "doobie-hikari" % versions.doobieVersion,
        "org.tpolecat" %% "doobie-postgres" % versions.doobieVersion,
        "org.tpolecat" %% "doobie-specs2" % versions.doobieVersion,
        "org.tpolecat" %% "doobie-scalatest" % versions.doobieVersion % "test"
      )
    }

    val flyway: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "org.flywaydb" % "flyway-core" % versions.flywayVersion
      )
    }

    val enumeratum: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "com.beachape" %% "enumeratum-doobie" % versions.enumeratum
      )
    }

    val csv: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "com.github.tototoshi" %% "scala-csv" % "1.3.6"
      )
    }

    val swagger: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "org.webjars" % "webjars-locator" % "0.40",
        "org.webjars" % "swagger-ui" % "3.25.3"
      )
    }

    val `dom-test-utils`: Def.Initialize[Seq[ModuleID]] = Def.setting {
      Seq(
        "com.raquo" %%% "domtestutils" % versions.`dom-test-utils` % Test
      )
    }
  }

  val sharedDependencies: Def.Initialize[Seq[ModuleID]] = Def.setting {
    Seq.concat(
      libs.circe.value
    )
  }

  val backendDependencies: Def.Initialize[Seq[ModuleID]] = Def.setting {
    Seq.concat(
      libs.http4s.value,
      libs.oauth2.value,
      libs.`typesafe-config`.value,
      libs.pureconfig.value,
      libs.doobie.value,
      libs.flyway.value,
      libs.enumeratum.value,
      libs.csv.value,
      libs.swagger.value,
      libs.logging.value
    )
  }

  val frontendDependencies: Def.Initialize[Seq[ModuleID]] = Def.setting {
    Seq.concat(
      libs.laminar.value,
      libs.`url-dsl`.value,
      libs.waypoint.value,
      libs.`dom-test-utils`.value,
    )
  }

}
